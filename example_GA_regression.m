%% Solve a linear regression problem using FlexiGA
% Max Flynn
% 4/11/2022

% Be sure to check *all* sections of this script to see what is going into
% the alg

clear all
close all;
clc

% expose the pre-existing functions so that you can select them
addpath(genpath('GeneticAlgorithm'));

%% Termination conditions 
max_gens = 1000; % Exit after this many generations
max_time = 30; % in seconds; quit out if clock time exceeds this amount
max_gens_no_better = 100; % if fitness stagnates for this many generations in a row, quit-out


%% Develop fitness function
% Wrap the pre-given mean-squared error function
% which treats "model" as a polynomial model: y = a*x1 + b*x2 + c*x3
N_x = 10;
N_data = 50;
solution = (1 - 0)*rand(1, N_x) + 0;
x = (1 - 0)*rand(N_data, N_x) + 0;

model_func =@(coeffs, x) x*coeffs'; pause(1);
y = model_func(solution, x);

fitness_func = (@(population)  fitness_mean_squared_error(population, model_func, y, x));

%% Population method
% you're interested, in this case, in numbers between 0 and 10
% use populate_random (you should change the mutation method, in this case)
N_pop = 25;    % number of population members
populator_func = (@(sz) populate_random_uniform_range(sz, 0, 1));
seed_pop = populator_func([N_pop, N_x])*NaN;

%% Crossover method
method = 'Mix';
func =@(sz) rand(sz);
crossover_func = (@(select, N_children) crossover_gene_bitwise(select, N_children, method, func));
N_parents = guess_N_parents(N_pop); % How many parents to select from each generation

%% Define mutation method
mut_rate = guess_mut_rate(N_pop); % frequency of bit mutation
mutate_func = (@(population) mutate_new_pop(population, mut_rate, populator_func));

%% Island method
N_islands = 8; %
migration_interval = 10; % every X gens, migrate best across islands

do_use_parallelism = 0; % if 1, do allow parallelism to solve fitnesses
% parallelism tends to be a net-negative for this case because
% the model is computationally "cheap"

%% Whether you want islands to be elitist or not
do_use_elitism = 1;

%% Fill parameters object
GAParameters = geneticAlgorithmParameters();
GAParameters.seed_pop = seed_pop;
GAParameters.N_parents = N_parents;

GAParameters.max_gens = max_gens;
GAParameters.max_time = max_time;
GAParameters.max_gens_no_better = max_gens_no_better;

GAParameters.use_elitism = do_use_elitism;
GAParameters.use_parallelism = do_use_parallelism;

GAParameters.populator_func = populator_func;
GAParameters.fitness_func = fitness_func;
GAParameters.crossover_func = crossover_func;
GAParameters.mutate_func = mutate_func;

GAParameters.N_islands = N_islands;
GAParameters.migration_interval = migration_interval;

GAParameters.do_output_statements = 1; % remember that printing statements is its own slowdown for this kind of problem
GAParameters.do_plot_progress = 0; % very slow in this case

%% Perform solution 


[ GAOutput ] = geneticAlgorithm( GAParameters );


[best_fitness, best_i] = max([GAOutput.island.best_fitness]);

f = model_func(GAOutput.island(best_i).best_individual, x);

figure;
hold on
plot(x, y, 'ob')
plot(x, f, 'rx')
xlabel('(x)')
ylabel('(y)')
legend('Exact', 'Model')

