%% Solve the knapsack problem using FlexiGA
% Max Flynn
% 4/11/2022
% Be sure to check *all* sections of this script to see what is going into
% the alg

clear all
close all
clc

% expose the pre-existing functions so that you can select them
addpath(genpath('GeneticAlgorithm'));

%% Termination conditions 
max_gens = 1000; % Exit after this many generations
max_time = 30; % in seconds; quit out if clock time exceeds this amount
max_gens_no_better = 100; % if fitness stagnates for this many rounds in a row, quit-out


%% Develop fitness function
% Wrap the pre-given knapsack fitness method with your set of weights vs
% values
N_items = 12;  % number of items to select from
carry_limit = 100;    % Above this threashold, fitness_knapsack calls this a "terrible" solution
weights = randi([1,40], 1, N_items);
values = randi([5, 150], 1, N_items);
fitness_func = (@(population) fitness_knapsack(population, weights, values, carry_limit));


%% Population method
% you're interested, in this case, in ones and zeros.
% use populate_random (you should change the mutation method, in this case)
N_pop = 10;    % number of population members (in one island)
populator_func = (@(sz) populate_random_bit(sz, 0, 1));
seed_pop = populator_func([N_pop, N_items])*NaN; % fill with nan to use random


%% Crossover method
N_loci = 2; % split genome into N_loci + 1 segments 
method = 'Swap';
func =@(sz) rand(sz);
crossover_func = (@(select, N_children) crossover_gene_locus(select, N_children, method, func, N_loci));
N_parents = guess_N_parents(N_pop); % How many parents to select from each generation


%% Define mutation method
mut_rate = guess_mut_rate(N_pop); % frequency of bit mutation
mutate_func = (@(population) mutate_bit_flip(population, mut_rate)); % bitFlip means swap the ones and zeros

%% Island method

N_islands = 4; %
migrationa_interval = 4; % every X gens, migrate best across islands

do_use_parallelism = 0; % if 1, do allow parallelism to solve fitnesses
% parallelism tends to be a net-negative for this case because
% the model is computationally "cheap"

%% Whether you want islands to be elitist or not

do_use_elitism = 1;

%% Fill parameters object

GAParameters = geneticAlgorithmParameters();
GAParameters.seed_pop = seed_pop;
GAParameters.N_parents = N_parents;

GAParameters.max_gens = max_gens;
GAParameters.max_time = max_time;
GAParameters.max_gens_no_better = max_gens_no_better;

GAParameters.use_elitism = do_use_elitism;
GAParameters.use_parallelism = do_use_parallelism;

GAParameters.populator_func = populator_func;
GAParameters.fitness_func = fitness_func;
GAParameters.crossover_func = crossover_func;
GAParameters.mutate_func = mutate_func;

GAParameters.N_islands = N_islands;
GAParameters.migration_interval = migrationa_interval;

GAParameters.do_output_statements = 1; % remember that printing statements is its own slowdown for this kind of problem
GAParameters.do_plot_progress = 1;

%% Perform solution 


[ GAOutput ] = geneticAlgorithm( GAParameters );


[best_fitness, best_i] = max([GAOutput.island.best_fitness]);
best_fitness

figure
imagesc(GAOutput.island(best_i).best_individual)
