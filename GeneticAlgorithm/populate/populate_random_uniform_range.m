%% Populate as random in range, uniformly distributed
% sz ==> size
% 2/3/2022
% Max Flynn

function [pop] = populate_random_uniform_range(sz, min, max)
    pop = (max - min)*rand(sz) + min;
end
