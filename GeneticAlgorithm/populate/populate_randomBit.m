%% Populate as bitwise, 0-1-0-0-0-1-1....
% sz ==> size
% 2/3/2022
% Max Flynn

function [pop] = populate_randomBit(sz, min, max)
    pop = randi([min, max], sz(1), sz(2));
end