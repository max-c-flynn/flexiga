%% mutations occur both with the same method as a new population and with a small perturbation
% pop : population which might mutate
% mut_rate : random chance for mutation
% chromo_func : pass in the generator for mutated chromosomes
% Max Flynn
% 04/13/2022

function [pop] = mutate_hard_soft(pop, mut_rate_hard, mut_rate_soft, chromo_func, perturb)
    
    if mut_rate_hard == 0 && mut_rate_soft == 0
        return;
    end
    
    % generate a fresh new population with chromo_func
    mutagen_hard = chromo_func(size(pop));
    mutagen_soft = pop  .* ((2*perturb)*rand(size(pop)) + (1-perturb)); 
    
    
    % random allayance of one's, zeros
    [i_hard, j_hard] = find(randi([1, ceil(1/mut_rate_hard)], size(pop,1), size(pop,2)) == 1);
    [i_soft, j_soft] = find(randi([1, ceil(1/mut_rate_soft)], size(pop,1), size(pop,2)) == 1);
    
    
    % on the random chance, swap in old chromosomes for the mutated ones
     pop(sub2ind(size(pop), i_hard, j_hard)) = mutagen_hard(sub2ind(size(pop), i_hard, j_hard));
     pop(sub2ind(size(pop), i_soft, j_soft)) = mutagen_soft(sub2ind(size(pop), i_soft, j_soft));
     
     % truncate
     pop(pop < 0) = 0;
     pop(pop > 1) = 1;
end