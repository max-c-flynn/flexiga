%% mutations occur with the same method as a new population
% pop : population which might mutate
% mut_rate : random chance for mutation
% chromo_func : pass in the generator for mutated chromosomes
% Max Flynn
% 02/02/2022

function [pop] = mutate_newPop(pop, mutRate, chromo_func)
    
    if mutRate == 0
        return;
    end
    
    % generate a fresh new population with chromo_func
    mutagen = chromo_func(size(pop));
    
    % random allayance of one's, zeros
    [i, j] = find(randi([1, ceil(1/mutRate)], size(pop,1), size(pop,2)) == 1);
    
    % on the random chance, swap in old chromosomes for the mutated ones
     pop(sub2ind(size(pop), i, j)) = mutagen(sub2ind(size(pop), i, j));
end