%% mutations occur with the same method as a new population
% pop : population which might mutate
% mut_rate : random chance for mutation
% chromo_func : pass in the generator for mutated chromosomes
% Max Flynn
% 02/02/2022



function [pop] = mutate_bit_flip(pop, mutRate)
    
    if mutRate == 0
        return;
    end

    
    % random allayance
    [i, j] = find(randi([1, ceil(1/mutRate)], size(pop,1), size(pop,2)) == 1);
    
    % invert bits
     pop(sub2ind(size(pop), i, j)) = ~pop(sub2ind(size(pop), i, j)); %<- logical ones and zeros only!
end