%% Seek the largest price without going over the carry limit
% by convention, y is the data per the model you are trying to match, and f is the
% result of *your* model, from pop
% idx returns the order of the most-to-least fit population
%
% Max Flynn
% 2/3/2022

function [fitness, idx, universalFitness] = fitness_knapsack(pop, weights, values, carryLimit)
    fitness = zeros(size(pop,1), 1);
    
    for i = 1:size(pop,1)
        w = sum(pop(i, :) .* weights, 'all');

        if w > carryLimit
            %fitness(i) = -1 * w; % max punishment for any over-limit: negative weight
            fitness(i) = 0;
        else
            fitness(i) = sum(pop(i, :) .* values, 'all'); % reward: accurate price for pack
        end
    end

    [~, idx] = sort(fitness, 'descend'); % best fitness is always the largest
     universalFitness = fitness; % highest is best, always
end