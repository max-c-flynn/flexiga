%% Seek the smallest mean absolute error
% by convention, y is the data per the model you are trying to match, and f is the
% result of *your* model, from pop
% idx returns the order of the most-to-least fit population
%
% Max Flynn
% 2/3/2022

function [fitness] = fitness_mean_absolute_error(pop, model, y, x)

    MAE = zeros(size(pop,1), size(y, 2));
    
    for i = 1:size(pop,1)
        
        % note: if f represents multiple outputs, it has more than 1 column
        f = model(pop(i,:), x); % result of model over points x
               
        MAE(i, :) = mean(abs(y-f) ./ abs(y), 1); % total sum of residuals, as percents [x/1]
    end
    
    if size(MAE, 2) > 1
        fitness = vecnorm(MAE')'; % get norm of rows of MAE
    else
        fitness = MAE;
    end

    fitness = 1./MAE;

end