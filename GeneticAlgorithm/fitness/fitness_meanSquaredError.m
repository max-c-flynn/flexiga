%% Seek the smallest mean squared error
% by convention, y is the data per the model you are trying to match, and f is the
% result of *your* model, from pop
% idx returns the order of the most-to-least fit population
%
% Max Flynn
% 2/3/2022

function [fitness] = fitness_meanSquaredError(pop, model, y, x)
    MSE = zeros(size(pop,1), 1);
    for i = 1:size(pop, 1)
        f = model(pop(i,:), x); % result of model over points x
        MSE(i) = sum(1/size(y,1) * (y-f).^2, 'all'); % total sum of mean squared error
    end
    %[~, idx] = sort(MSE, 'ascend'); % best MSE is always the littlest

    fitness = 1./ MSE;

    %universalFitness = 1 ./ MSE; % highest is best, always
end