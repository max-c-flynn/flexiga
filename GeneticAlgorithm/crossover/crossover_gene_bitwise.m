%% swap bit-by-bit with a% chance
% select : most-fit parents to be propogated 
% N_children : how many children to generate
% swap_chance_func : generate [0,1] chance of swap
% method : keyword, 'Swap' 'Mix' 'Product'
% func : generate weight of mix, or % chance of swap
%
% Max Flynn
% 02/02/2022

function [children] = crossover_gene_bitwise(select, N_children, method, func)

    mates = select_mates(select, N_children); % should be as tall as N_children, as wide as N_parents
    children = zeros(N_children, size(select, 2))*NaN;
    
    for i = 1:N_children
        m = mates(i, :);
        
        parent1 = select(m(1), :);
        parent2 = select(m(2), :);
        
        if strcmp(method, 'Swap')
            swaps = randi([0, ceil(1/func(1))-1], size(parent1));
            child = parent1;
            child(swaps==0) = parent2(swaps==0);
            
        elseif strcmp(method, 'Mix')
            mix = func(size(parent1));
            child = parent1 .* mix + parent2 .* (1 - mix);
            
        elseif strcmp(method, 'Product')
            child = parent1 .* parent2;
            
        elseif strcmp(method, 'GeometricMean')
            child = sqrt(parent1 .* parent2);
            
        else
            error('Unrecognized method');
        end

        children(i,:) = child;
    end
end