%% Select mate pairs in prep for crossover
% select : most-fit parents to be propogated 
% N_children : how many children to generate
%
% Max Flynn
% 02/02/2022

function [mates] = selectMates(select, N_children)
    mates = nchoosek((1:size(select, 1)), 2);           % get all combinations
    
    % double up some of the pairs if necessary
    if size(mates, 1) < N_children
    %    N_missing = N_children - size(mates, 1);

        while size(mates, 1) < N_children
            mates = [mates; mates];
        end
    end

    % must elect to swap let and right sides, keep parent1 from dominating
    swaps = randi([0,1], size(mates, 1), 1);
    mates(swaps==1, :) = [mates(swaps==1,2), mates(swaps==1, 1)];

    % shuffle the rows
    if size(mates, 1) > 1
        mates = mates(randperm(size(mates,1)),:);           
    end

    mates = mates(1:N_children, :);    % now the first N is random, grab them

end