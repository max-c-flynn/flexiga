%% Crossover based on locus partitions
% select : most-fit parents to be propogated 
% N_children : how many children to generate
% swap_chance_func : generate [0,1] chance of swap
% method : keyword, 'Swap' 'Mix' 'Product'
% func : generate weight of mix, if valid
%
% now: for Swap, swap-chance is guaranteed
%
% Max Flynn
% 02/02/2022
 
 function [children] = crossover_gene_locus(selected, N_children, method, func, N_loci)
    
    mates = selectMates(selected, N_children); % should be as tall as N_children, as wide as N_parents
    children = zeros(N_children, size(selected, 2))*NaN;
    
    for i = 1:N_children
        m = mates(i, :);
        
        parent1 = selected(m(1), :);
        parent2 = selected(m(2), :);
        
        % find ascending index points seperating the chunks
        loci = randperm(size(selected,2)-1);
        loci = sort(loci(1:N_loci));
        
            child = zeros(size(parent1));
            mix = func(1);
            
            startpt = 1;
            for n = 1:(N_loci+1)
                
                if n==N_loci+1
                    endpt = length(parent1);
                else
                    endpt = loci(n);
                end
                
                 if strcmp(method, 'Swap')
                    child(startpt:endpt) = parent1(startpt:endpt);
                    
                 elseif strcmp(method, 'Mix')
                     
                     child(startpt:endpt) = parent1(startpt:endpt) .* mix + parent2(startpt:endpt) .* (1-mix);
                     
                 end
                
                 [parent1, parent2] = deal(parent2, parent1); % change which parent is dominant on next locus
                 
                if n ~= N_loci+1
                    startpt = loci(n) + 1;
                end
            end
       
        children(i,:) = child;
    end
 
 end