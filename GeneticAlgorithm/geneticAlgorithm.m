%% Structure for a generalizable genetic algorithm


function [ out ] = geneticAlgorithm(param)
     
    FIG_POP = length(findobj('type','figure'))+1;
    FIG_FIT = FIG_POP + 1;
    figures = {};
    warning('off', 'MATLAB:Figure:FigureSavedToMATFile')

    disp_ga(param, "Entered geneticAlgorithm");
    
    %% Begin evolution through generations
    N_gen = 0;

    if param.continue_save_state == 1
        disp_ga(param, sprintf("Continuing results from saved state '%s'", param.save_state_name));
        load(param.save_state_name); % just continue generations presumed paused in the saved state
        N_gen = N_gen+1; % necessary to not double-over the most recent gen
    else
        
        out = geneticAlgorithmOutput();
        out.parameters = param;
       
        % determine either random or seeded initial population
        USE_RANDOM_SEED_POP = 0;
        if any(isnan(param.seed_pop))
            USE_RANDOM_SEED_POP = 1;
        end

        out.island(1:param.N_islands) = geneticAlgorithmIsland();

        

        if param.use_parallelism == 1
            p = gcp;
            M = p.NumWorkers;
            disp_ga(param, sprintf("M in parfor is: %d", M));
        else
            M = 0;
        end


        % initialize islands
        for i = 1:param.N_islands

                %if numGen == 1
                    if USE_RANDOM_SEED_POP
                        out.island(i).population ...
                            = param.populator_func(size(param.seed_pop));
                    else
                        out.island(i).population = param.seed_pop;
                    end

                    out.island(i).best_fitness = NaN;
                    out.island(i).best_fitness_history = [NaN, NaN];
                    out.island(i).current_best_fitness_history = [NaN, NaN];
                    out.island(i).best_individual_history = NaN(1, width(param.seed_pop));

                    out.island(i).initial_population = ...
                        out.island(i).population;
                     out.island(i).fitness = zeros(height(param.seed_pop), 1);

        end

        gens_no_better = 0; % number of gens since a better member has been found in any island
        out.calculation_time = [];

        % fill in towards back
        ELITIST_PLACE = ((height(param.seed_pop) - param.use_elitism + 1) : height(param.seed_pop));
        MIGRATE_PLACE = min(ELITIST_PLACE)-1;
        N_gen = 1;
    end
    
    if all([isnan(param.max_fitness), isnan(param.max_gens_no_better), ...
            isnan(param.max_time), isnan(param.max_gens)])
            disp_ga(param, 'Warning: param comes with no stop-criterion. GA should not be expected to end on its own terms.')
    end
    
    %% set up for plots
if param.do_plot_progress == 1
            % init figs
            for i = [FIG_POP, FIG_FIT]
                 figures{i} = figure(i);
                 clf
                 set(figure(i),'WindowStyle','docked')
                 set(figure(i),'color','w')
            end

            set(figure(FIG_POP), 'Name', 'Population')
            set(figure(FIG_FIT), 'Name', 'Fitness')

            c = colororder('default');
            for i = 1:param.N_islands
                c = [c; c];
            end

             
                if param.N_islands == 1
                    figure(FIG_POP)
                     pcolor(out.island(1).population)
                     hold on
                     xlim([1, width(param.seed_pop)])
                     ylim([1, height(param.seed_pop)])

                    figure(FIG_FIT)
                    hold on
                    plot(out.island.current_best_fitness_history(:,1), out.island.current_best_fitness_history(:,2), 'o', 'color' , c(1, :))

                else
                    figure(FIG_POP)
                    X = 2;
                    Y = ceil(param.N_islands / X);
                    for i = 1:param.N_islands
                        subplot(Y, X, i);
                        pcolor(out.island(i).population)
                        hold on
                        xlim([1, width(param.seed_pop)])
                        ylim([1, height(param.seed_pop)])
                        title(sprintf('Island %d', i))
                    end

                    figure(FIG_FIT)
                    hold on
                    xlabel('Generation')
                    ylabel('Fitness')
                    for i = 1:param.N_islands
                        plot(out.island(i).current_best_fitness_history(:,1), out.island(i).current_best_fitness_history(:,2), 'o', 'color', c(i, :))
                        %plot(out.island.best_fitness_history(:,1), out.island.best_fitness_history(:,2), 'rv')
                    end

                end

end

%% % Keep going until a quit condition has been reached
    while true
    
        % Quit if too many gens have passed.
        if ~isnan(param.max_gens) && N_gen > param.max_gens
            break
        end

        disp_ga(param, sprintf("Generation: %d", N_gen));
         
        % begin gen clock timer
        timer = tic;
        

        
        %% Fitness evaluation

        N_tasks = height(param.seed_pop) * param.N_islands;
        fit_arr = nan(N_tasks, 1);
        
        % speedups: prevent broadcast variables
        % 10/06/2024 - was a great learning experience doing this in
        % MATLAB, but this turned out to be one of those very substantial
        % reasons to do this in any other language
        foo =@ param.fitness_func;
        pop_pool = [];
        idx_island = [];
        idx_ind = [];
        for i_island = 1:param.N_islands
            pop_pool = [pop_pool; out.island(i_island).population]; % the actual individual
            idx_island = [idx_island; ones(height(out.island(i_island).population), 1) * i_island]; % island number assosciated
             idx_ind = [idx_ind; (1:height(out.island(i_island).population))']; % individual index inside the island
        end
        
        if param.use_parallelism == 1
            p = gcp;
            M = p.NumWorkers;
        else
            M = 0;
        end
        
        
        % in parallel (if chosen), find fitnesses
        if  M == 0 || M == 1
            for i_jobID = 1:N_tasks   % non-parallel style, this lets you pause to debug better
                ind = pop_pool(i_jobID, :);
                 fit_arr(i_jobID) = foo(ind);
            end
        else
            parfor (i_jobID = 1:N_tasks, M)    % do use parallel-style
                ind = pop_pool(i_jobID, :);
                 fit_arr(i_jobID) = foo(ind);
            end
        end
        
        % unwrap the results of the parfor into the islands and individuals
        for i_jobID = 1:N_tasks
             out.island(idx_island(i_jobID)).fitness(idx_ind(i_jobID)) = fit_arr(i_jobID);
             if isnan(fit_arr(i_jobID))
                 out.island(idx_island(i_jobID)).fitness(idx_ind(i_jobID)) = 0;
             end
             
        end   
        

        % order islands based on fitness
        for i = 1:param.N_islands

            [~, best_i] = sort(out.island(i).fitness, 'descend');
            out.island(i).fitness = out.island(i).fitness(best_i);
            out.island(i).population = out.island(i).population(best_i, :);
        
            [~, best_i] = max(out.island(i).fitness);
            % before elitism or migration, figure out the natural best achieved
            out.island(i).current_best_fitness = out.island(i).fitness(best_i);
             out.island(i).current_best_individual = out.island(i).population(best_i, :);
        
            % update current fitness history
                    hist_idx = height(out.island(i).current_best_fitness_history)+1;
                    out.island(i).current_best_fitness_history(hist_idx, 1) = N_gen;
                    out.island(i).current_best_fitness_history(hist_idx, 2) ...
                        = out.island(i).current_best_fitness;
        end

        % from this, figure out if a new best has appeared anywhere
        NEW_BEST_FOUND = false;
        ISLAND_SHOULD_IGNORE_ELITISM = [];
        for i = 1:param.N_islands  

                % update recording of best fitness
                if out.island(i).current_best_fitness > out.island(i).best_fitness || N_gen == 1

                    out.island(i).best_fitness = out.island(i).current_best_fitness;
                    out.island(i).best_individual = out.island(i).current_best_individual;

                    % update fitness history
                    hist_idx = height(out.island(i).best_fitness_history)+1;
                    out.island(i).best_fitness_history(hist_idx, 1) = N_gen;
                    out.island(i).best_fitness_history(hist_idx, 2) ...
                        = out.island(i).best_fitness;
                    
                    out.island(i).best_individual_history(hist_idx, :) = out.island(i).best_individual;
                    
                    NEW_BEST_FOUND = true;
                    ISLAND_SHOULD_IGNORE_ELITISM(end+1) = i; % when was found, elitism would cause double-up
                end
        end
        if NEW_BEST_FOUND == false
            gens_no_better = gens_no_better + 1;
            
            if gens_no_better > param.max_gens_no_better
                break;
            end
        else
            gens_no_better = 0;
        end
        

        %% Elitism trigger -  place elites as valid
        % replace one of the poor individuals with the elite before
        % crossover (fitnesses currently ordered)
         N_elites = min([param.use_elitism, N_gen]);
         if param.use_elitism > 0 && N_gen > (N_elites-1)
            
             
             for i = 1:param.N_islands
                        if any(i == ISLAND_SHOULD_IGNORE_ELITISM)
                            continue; % avoid doubling up!
                        end

                        this_N_elites = min([N_elites, height(out.island(i).best_fitness_history)-1]); % bandage

                        elite_fitnesses = out.island(i).best_fitness_history((end-this_N_elites+1):end)';
                        elite_individuals = out.island(i).best_individual_history((end-this_N_elites+1):end, :);
                        
                            out.island(i).population(ELITIST_PLACE((end-this_N_elites+1):end),:) ...
                                    = elite_individuals;
                            out.island(i).fitness(ELITIST_PLACE((end-this_N_elites+1):end)) ...
                                    = elite_fitnesses;
              end
         end

        % re-order to prevent migration from screwing up with the
        % island-copy
         for i = 1:param.N_islands
             % re-order based on fitness
                [~, best_i] = sort(out.island(i).fitness, 'descend');
               out.island(i).fitness = out.island(i).fitness(best_i);
               out.island(i).population = out.island(i).population(best_i, :);
         end
        
        %% migration trigger 
         % copy the best individuals over to other islands
         if mod(N_gen, param.migration_interval) == 0 && param.N_islands > 1 && ~isempty(MIGRATE_PLACE)

            dest = (1:param.N_islands)'; % where the kth island currentBest will migrate to, if not-redundant 
            dest = dest(randperm(size(dest,1)),:);  

            islands_copy = out.island; % safekeeping copy
             for i = 1:param.N_islands
                 if i == dest(i)
                    continue; % skip over
                 end
                    out.island(i).population(MIGRATE_PLACE, :) ...
                         = islands_copy(dest(i)).current_best_individual;
                  out.island(i).fitness(MIGRATE_PLACE) ...
                         = islands_copy(dest(i)).current_best_fitness;
             end
         end

        %% Crossover and Mutation
           % Use the results of the model to create next gen (in serial)
           for i = 1:param.N_islands  
                
               % order based on fitness
                [~, best_i] = sort(out.island(i).fitness, 'descend');
                out.island(i).fitness = out.island(i).fitness(best_i);
                out.island(i).population = out.island(i).population(best_i, :);

                selected = out.island(i).population(1:param.N_parents, :);

                % generate and mutate new population
                out.island(i).population ...
                        = param.crossover_func(selected, height(param.seed_pop));
                out.island(i).population ...
                        = param.mutate_func(out.island(i).population);
                out.island(i).fitness(:) = NaN; % nan-out because fitness no longer matches the present pop
                    
                if N_gen == param.max_gens
                        out.island(i).final_population = out.island(i).population;
                        out.island(i).final_fitness =  out.island(i).fitness;
                        out.island(i).final_gen = N_gen;
                end
           end
        
           out.calculation_time(end+1) = toc(timer);
           
           disp_ga(param, ...
               sprintf("Generation: %d concluded (%1.4e s, current best f=%1.4e, best f=%1.4e)",...
               [N_gen, toc(timer), max([out.island.current_best_fitness]), max([out.island.best_fitness])]));
           
           if param.do_plot_progress == 1
                plot_progress_update(FIG_POP, FIG_FIT, {out.island.population}, {out.island.current_best_fitness_history}, N_gen, c)
           end

           % SaveStates activation
           if param.use_save_states == 1
               save(param.save_state_name);
           end
           
           if N_gen >= param.max_gens
               disp_ga(param, 'Exiting: Maximum generations reached.')
               break;
           end
           
            if max([out.island.best_fitness]) >= param.max_fitness
               disp_ga(param, 'Exiting: Maximum target fitness in best individual reached.')
               break;
            end
            
            if ~isnan(param.max_time) % can be expensive to do every time?
                if sum(out.calculation_time) >= param.max_time
                   disp_ga(param, 'Exiting: Maximum clock time in evolution reached.')
                   break;
                end
            end
           
           if gens_no_better >= param.max_gens_no_better
               disp_ga(param, 'Exiting: Stagnation in evolution reached.')
               break;
           end
           
           N_gen = N_gen + 1;
    end 

    disp_ga(param, 'Leaving genetic algorithm.');
end

%% display otuput statements wrapper
function disp_ga(param, str)
    if param.do_output_statements == 1
        disp(strcat("ga: ", str));
    end
end

%% plot progress per-generation
function plot_progress_update(FIG_POP, FIG_FIT, array_islands, array_histories, N_gen, c)

    figure(FIG_POP)
    if length(array_islands) == 1
        cla
        pcolor(array_islands{1})
    else
        for i = 1:length(array_islands)
            X = 2;
            Y = ceil(length(array_islands) / X);
            subplot(Y, X, i)
            cla
            pcolor(array_islands{i})
        end
    end

    figure(FIG_FIT)
    if length(array_islands) == 1
            hist = array_histories{1};
            plot(hist(end,1), hist(end,2), 'o', 'color', c(1, :))
        else
            for i = 1:length(array_islands)
                hist = array_histories{i};
                 plot(hist(end,1), hist(end,2), 'o', 'color', c(i, :))
            end
        end
   

end


