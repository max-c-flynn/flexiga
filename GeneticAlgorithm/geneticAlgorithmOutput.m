classdef geneticAlgorithmOutput
   properties
      parameters
      island geneticAlgorithmIsland
      calculation_time
   end
   methods
      function obj = geneticAlgorithmIsland(parameters)
        obj.parameters = parameters;
        obj.islands(1:obj.parameters.N_islands) ...
            = geneticAlgorithmIsland();
      end
   end
end
