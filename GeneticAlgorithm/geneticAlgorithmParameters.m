
classdef geneticAlgorithmParameters

   properties

      seed_pop

      N_parents

      N_islands
      migration_interval

      max_gens
      max_time
      max_gens_no_better
      max_fitness

      use_elitism
      use_parallelism
      use_save_states

      do_output_statements
      do_plot_progress
      continue_save_state

      populator_func
      model_func
      fitness_func
      crossover_func
      mutate_func
      
      save_state_name

      
   end
   methods
      ...
   end

end
