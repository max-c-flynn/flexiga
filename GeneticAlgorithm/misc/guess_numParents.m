% Max Flynn
% 5/8/2023
% fast function for the num of parent matches necessary to full X children per gen

function [numParents] = guess_numParents(desired_numChildren)

    for i = 2:desired_numChildren
        target = height(nchoosek(1:i, 2));
        if target >= desired_numChildren
            break
        end
    end
    
    numParents = i;

end