% Max Flynn
% 5/8/2023
% emprically okay choice for mutRate if you don't have a better idea

function [mutRate] = guess_mutRate(numPop)

    mutRate = 1.0 / numPop;

    mutRate = min(mutRate, 0.25);
    mutRate = max(mutRate, 0.01);

end