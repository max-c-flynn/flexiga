% Max Flynn
% 5/8/2023
% fast function for the num of parent matches necessary to full X children per gen

function [N_parents] = guess_N_parents(desired_N_children)

    for i = 2:desired_N_children
        target = height(nchoosek(1:i, 2));
        if target >= desired_N_children
            break
        end
    end
    
    N_parents = i;

end