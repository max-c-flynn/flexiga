% Max Flynn
% 5/8/2023
% emprically okay choice for mutRate if you don't have a better idea

function [mut_rate] = guess_mut_rate(N_pop)

    mut_rate = 1.0 / N_pop;

    mut_rate = min(mut_rate, 0.25);
    mut_rate = max(mut_rate, 0.01);

end