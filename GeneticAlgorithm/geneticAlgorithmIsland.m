% Max Flynn
% 5/9/2023

classdef geneticAlgorithmIsland

   properties
      current_best_individual
      current_best_fitness
      best_individual
      best_fitness
      initial_population
      final_population
      final_fitness
      final_gen
      
      best_fitness_history
      best_individual_history
      
      current_best_fitness_history

      population
      fitness
      
   end
   methods
       function obj = geneticAlgorithmIsland()
         ...
      end
   end

end
